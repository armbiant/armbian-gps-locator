# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2021-2022
* Konstantin Samorodov
  * Developer, 2021
* Pavel Kazeko, <p.kazeko@omp.ru>
  * Developer, 2021-2022
  * Maintainer, 2021-2022
* Vasiliy Rychkov, <v.rychkov@omp.ru>
  * Icon Designer, 2022
* Alexey Andreev, <a.andreev@omp.ru>
  * Reviewer, 2021-2022
