/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Geolocation project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import QtLocation 5.0
import QtPositioning 5.2
import Sailfish.Silica 1.0
import ru.auroraos.Geolocation 1.0

Page {
    property GpsInfoProvider gpsInfoProvider

    objectName: "locationInfoPage"

    SilicaListView {
        objectName: "listView"
        anchors.fill: parent
        spacing: Theme.paddingLarge
        header: PageHeader {
            objectName: "pageHeader"
            title: qsTr("Location information")
        }
        model: [{
                "name": "GPS",
                "value": gpsInfoProvider.gpsEnabled ? qsTr("On") : qsTr("Off")
            }, {
                "name": qsTr("Position source"),
                "value": gpsInfoProvider.positioningMethod
            }, {
                "name": qsTr("Latitude"),
                "value": gpsInfoProvider.coordinate.latitude
            }, {
                "name": qsTr("Longitude"),
                "value": gpsInfoProvider.coordinate.longitude
            }, {
                "name": qsTr("Altitude"),
                "value": gpsInfoProvider.coordinate.altitude
            }, {
                "name": qsTr("Vertical accuracy"),
                "value": gpsInfoProvider.verticalAccuracy >= 0 ? gpsInfoProvider.verticalAccuracy : "-"
            }, {
                "name": qsTr("Horizontal accuracy"),
                "value": gpsInfoProvider.horizontalAccuracy >= 0 ? gpsInfoProvider.horizontalAccuracy : "-"
            }, {
                "name": qsTr("Speed"),
                "value": gpsInfoProvider.speed >= 0 ? "%1 %2 (%3)"
                                                      .arg(gpsInfoProvider.speed).arg(qsTr("m/s"))
                                                      .arg(gpsInfoProvider.timestamp.toLocaleTimeString())
                                                    : "-"
            }, {
                "name": qsTr("Direction"),
                "value": gpsInfoProvider.direction
            }]
        delegate: Row {
            objectName: "infoRow_%1".arg(index)
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingLarge

            Label {
                objectName: "nameLabel"
                text: modelData.name
                width: (parent.width - parent.spacing) / 2
                horizontalAlignment: Text.AlignRight
                wrapMode: Text.Wrap
            }

            Label {
                objectName: "valueLabel"
                text: modelData.value || "-"
                width: (parent.width - parent.spacing) / 2
                wrapMode: Text.Wrap
            }
        }
    }
}
