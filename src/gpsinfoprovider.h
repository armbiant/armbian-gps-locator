/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Geolocation project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef GPSINFOPROVIDER_H
#define GPSINFOPROVIDER_H

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QMap>
#include <QtPositioning/QGeoCoordinate>
#include <QtPositioning/QGeoPositionInfo>
#include <QtPositioning/QGeoSatelliteInfo>

#include "satelliteinfo.h"

class QGeoPositionInfoSource;
class QGeoSatelliteInfoSource;

class GpsInfoProvider : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(QVariantList satellites READ satellites NOTIFY satellitesChanged)
    Q_PROPERTY(qint32 satellitesInViewCount READ satellitesInViewCount NOTIFY satellitesChanged)
    Q_PROPERTY(qint32 satellitesInUseCount READ satellitesInUseCount NOTIFY satellitesChanged)
    Q_PROPERTY(QGeoCoordinate coordinate READ coordinate NOTIFY positionInfoChanged)
    Q_PROPERTY(QDateTime timestamp READ timestamp NOTIFY positionInfoChanged)
    Q_PROPERTY(qreal horizontalAccuracy READ horizontalAccuracy NOTIFY positionInfoChanged)
    Q_PROPERTY(qreal verticalAccuracy READ verticalAccuracy NOTIFY positionInfoChanged)
    Q_PROPERTY(QString direction READ direction NOTIFY positionInfoChanged)
    Q_PROPERTY(qreal speed READ speed NOTIFY positionInfoChanged)
    Q_PROPERTY(bool gpsEnabled READ gpsEnabled NOTIFY positionInfoChanged)
    Q_PROPERTY(QString positioningMethod READ positioningMethod NOTIFY positionInfoChanged)

public:
    explicit GpsInfoProvider(QObject *parent = nullptr);

    bool active() const;
    QVariantList satellites() const;
    qint32 satellitesInViewCount() const;
    qint32 satellitesInUseCount() const;
    QGeoCoordinate coordinate() const;
    QDateTime timestamp() const;
    qreal horizontalAccuracy() const;
    qreal verticalAccuracy() const;
    QString direction() const;
    qreal speed() const;
    bool gpsEnabled() const;
    QString positioningMethod() const;

    void setActive(bool active);

signals:
    void activeChanged();
    void positionInfoChanged();
    void satellitesChanged();

private slots:
    void _updatePositionInfo(const QGeoPositionInfo &info);
    void _updateSatellitesInUse(const QList<QGeoSatelliteInfo> &satellitesInfo);
    void _updateSatellitesInView(const QList<QGeoSatelliteInfo> &satellitesInfo);

private:
    void _appendSatellite(const QGeoSatelliteInfo &info, bool inUse);
    qreal _geoPositionAttribute(QGeoPositionInfo::Attribute attribute) const;

private:
    QGeoPositionInfoSource *m_positionSource { nullptr };
    QGeoSatelliteInfoSource *m_satelliteSource { nullptr };
    QGeoPositionInfo m_positionInfo {  };
    QMap<qint32, SatelliteInfo> m_satellites {  };
    bool m_active { false };
};

#endif // GPSINFOPROVIDER_H
