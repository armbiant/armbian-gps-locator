# Geolocation

The project provides the usage examples of the API that allows to work with geolocation:
positioning, satellites info, maps.

The main purpose is to show not only what features are available to work with these API,
but also how to use them correctly.

The source code of the project is provided under [the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

The project has a common structure of an application based on C++ and QML for Aurora OS.
All the C++ sources files are in [src](src) directory, QML sources are in [qml](qml) directory.

The features related to the examples are the following:
- [The satellite info](src/satelliteinfo.h) is a wrapper for QGeoSatelliteInfo; it is intended
for use in QML and it contains information about available satellites.
- [GPS info provider](src/gpsinfoprovider.h) is a collective class and provides all possible
information that can be obtained from the C++ classes: `QGeoPositionInfoSource` and
`QGeoSatelliteInfoSource`.
- The pictures used for the UI are located in the [qml/images](qml/images) directory.
- QML documents of application pages are located in the [qml/pages](qml/pages) directory.
- Information about the project and the license is displayed in [AboutPage.qml](qml/pages/AboutPage.qml) file.
- Application navigation is implemented in the [MapPage.qml](qml/pages/MapPage.qml) file.
